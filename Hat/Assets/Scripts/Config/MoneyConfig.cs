﻿using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace Game {

    [CreateAssetMenu(menuName = "Game/Configs/Money Config")]
    public sealed class MoneyConfig : ScriptableObjectSingleton<MoneyConfig> {
        [TabGroup("Money")]
        [ListDrawerSettings(ListElementLabelName = "EditorName")]
        public MoneyData[] MoneyData;
    }
}
