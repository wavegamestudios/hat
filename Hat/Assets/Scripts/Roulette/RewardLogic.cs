﻿using UnityEngine;
using UnityEngine.UI;

namespace Game {

    public class RewardLogic : MonoBehaviour {

        [SerializeField]
        private Stuff _stuff;

        public Stuff Stuff { get { return _stuff; } }
      
    }
}
