﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Game {
    public class RouletteLogic : AbstractLoader {

        [SerializeField]
        private Shop _shop;

        [SerializeField]
        private Bank _bank;

        [SerializeField]
        private RewardLogic[] _rewardSectors;

        [SerializeField]
        private Arrow _arrow;

        [SerializeField]
        private RouletteRotate _rouletteRotate;

        [SerializeField]
        private Wardrobe _wardrobe;

        [SerializeField]
        private Scrollbar _scrollbar;

        private void OnEnable() {
            InitReward();
        }

        private void Start() {
            _rouletteRotate.StopRouletteRotate += StopRotate;
            InitReward();
        }

        private void InitReward() {

            foreach (var reward in _rewardSectors) {
                var stuffList = FindStuffData(reward.Stuff);
                if (reward.Stuff != Stuff.none && stuffList.Count > 0) {
                    reward.gameObject.GetComponent<Image>().sprite = stuffList.First().sprite;
                }
            }
        }

        private void StopRotate() {

            var stuff = _arrow.LastSector.gameObject.GetComponent<RewardLogic>().Stuff;

            if (stuff == Stuff.Money || FindStuffData(stuff).Count <= 0) {
                LoadMoney(Stuff.Money);
                ReloadScrollBar();
                return;
            }

            if (stuff != Stuff.none) {
                LoadStuff(stuff);
                StartCoroutine(ReloadStuff());
                ReloadScrollBar();
            }
        }

        private void LoadMoney(Stuff stuff) {

            SelectConfig(stuff);

            foreach (var data in _configData) {
                if (DetectedStyffGroup(data) == stuff) {
                    gameObject.GetComponent<ContentRouleteLoader>().LoadCell(data);
                    _bank.SetMoney(_bank.GetMoney() + data.price);
                }
            }
        }

        private void LoadStuff(Stuff stuff) {

            var data = FindStuffData(stuff);

            if (data.Count > 0) {
                var firstData = data.First();
                _shop.MakePurchase(firstData);
                _wardrobe.WardrodeList.Add(DetectedKey(firstData), DetectedStyffGroup(firstData));
                gameObject.GetComponent<ContentRouleteLoader>().LoadCell(firstData);
            }
        }

        IEnumerator ReloadStuff() {
            yield return new WaitForSeconds(1.0f);
            InitReward();
        }

        private List<AbstractData> FindStuffData(Stuff stuffGroup) {

            List<AbstractData> _stuffDatas = new List<AbstractData>();

            foreach (var stuffKey in _shop.ShopList.Keys) {

                if (stuffGroup == _shop.ShopList[stuffKey]) {

                    SelectConfig(stuffGroup);

                    foreach (var data in _configData) {
                        if (DetectedKey(data) == stuffKey) {
                            _stuffDatas.Add(data);
                        }
                    }
                }
            }

            return _stuffDatas;
        }

        private void ReloadScrollBar() {
            StartCoroutine(CoroutineReloadScrollBar());
        }

        IEnumerator CoroutineReloadScrollBar() {
            yield return new WaitForSeconds(0.5f);
            _scrollbar.value = 1;
        }
    }
}
