﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Utils;

namespace Game {

    public sealed class AbstractEnumStorage : SingletonCrossScene<AbstractEnumStorage> {

        [SerializeField]
        private AbstractEnum[] _enums;

        public AbstractEnum[] GetEnumsList()
        {
            return _enums;
        }

        public Dictionary<Type, AbstractEnum> GetEnums() {
            var result = new Dictionary<Type, AbstractEnum>();
            for (int i = 0; i < _enums.Length; i++) {

                // Protection against null values
                if(_enums[i] == null)
                {
                    continue;
                }

                var type = _enums[i].GetType();
                if (result.ContainsKey(type)) {
                    //Logger.LogErrorFormat("Duplicate enum instance: {0}!", type.ToString());
                }
                result[type] = _enums[i];
            }
            return result;
        }
    }
}