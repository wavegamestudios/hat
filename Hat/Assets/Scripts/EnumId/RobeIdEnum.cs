﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;

namespace Game {

    [InlineProperty]
    [Serializable]
    public struct RobeId {

        [HideLabel]
        [ValueDropdown("GetValues", DisableListAddButtonBehaviour = false)]
        public int value;


        private static IList GetValues() {
            return AbstractEnum.GetDropdownValues<RobeIdEnum>();
        }

        public string GetName()
        {
            return AbstractEnum.GetElementNameByValue<RobeIdEnum>(value);
        }
    }

    [CreateAssetMenu(menuName = "Game/Enum/RobeId")]
    public sealed class RobeIdEnum : AbstractEnum {
    }
}