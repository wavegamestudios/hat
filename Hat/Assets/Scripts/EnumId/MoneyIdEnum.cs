﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;

namespace Game {

    [InlineProperty]
    [Serializable]
    public struct MoneyId {
    
        [HideLabel]
        [ValueDropdown("GetValues", DisableListAddButtonBehaviour = false)]
        public int value;

        private static IList GetValues() {
            return AbstractEnum.GetDropdownValues<MoneyIdEnum>();
        }

        public string GetName()
        {
            return AbstractEnum.GetElementNameByValue<MoneyIdEnum>(value);
        }
    }

    [CreateAssetMenu(menuName = "Game/Enum/MoneyId")]
    public sealed class MoneyIdEnum : AbstractEnum {
    }
}