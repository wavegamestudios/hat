﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;

namespace Game {

    [InlineProperty]
    [Serializable]
    public struct HatId {
    
        [HideLabel]
        [ValueDropdown("GetValues", DisableListAddButtonBehaviour = false)]
        public int value;

        private static IList GetValues() {
            return AbstractEnum.GetDropdownValues<HatIdEnum>();
        }

        public string GetName()
        {
            return AbstractEnum.GetElementNameByValue<HatIdEnum>(value);
        }
    }

    [CreateAssetMenu(menuName = "Game/Enum/HatId")]
    public sealed class HatIdEnum : AbstractEnum {
    }
}