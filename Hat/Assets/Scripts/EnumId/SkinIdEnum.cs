﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using UnityEngine;

namespace Game {

    [InlineProperty]
    [Serializable]
    public struct SkinId {

        [HideLabel]
        [ValueDropdown("GetValues", DisableListAddButtonBehaviour = false)]
        public int value;

        private static IList GetValues() {
            return AbstractEnum.GetDropdownValues<SkinIdEnum>();
        }

        public string GetName()
        {
            return AbstractEnum.GetElementNameByValue<SkinIdEnum>(value);
        }
    }

    [CreateAssetMenu(menuName = "Game/Enum/SkinId")]
    public sealed class SkinIdEnum : AbstractEnum {
    }
}