﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Game {

    [Serializable]
    class TabField {
        public RectTransform slider;
        public Toggle toggle;
    }

    public class ScreenOneTab : MonoBehaviour {

        [SerializeField]
        private List<TabField> _sliderList;

        private void OnEnable() {
            ActivateSlider();
        }

        private void Start() {
            InitTab();
        }

        private void InitTab() {
            foreach (var tab in _sliderList) {
                tab.toggle.onValueChanged.AddListener((isSelected) => {
                    if (!isSelected) return; 
                    ActivateSlider();
                });
            }
        }

        private void ActivateSlider() {
            AllSliderDisabled();
            GetActiveCategory().slider.GetComponent<Canvas>().enabled = true;
        }

        private TabField GetActiveCategory() {
            var activeCategory = _sliderList.FirstOrDefault(c => c.toggle.isOn == true);
            return activeCategory;
        }

        private void AllSliderDisabled() {
            foreach (var tab in _sliderList) {
                if (tab.slider.GetComponent<Canvas>().enabled)
                    tab.slider.GetComponent<Canvas>().enabled = false;
            }
        }
    }

}
