﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UI : MonoBehaviour {

    [SerializeField]
    GameObject _screenOne;

    [SerializeField]
    GameObject _screenTwo;

    private void LockAllScreen() {
        _screenOne.SetActive(false);
        _screenTwo.SetActive(false);
    }

    public void LoadScreenOne() {
        LockAllScreen();
        _screenOne.SetActive(true);
    }

    public void LoadScreenTwo() {
        LockAllScreen();
        _screenTwo.SetActive(true);
    }

}
