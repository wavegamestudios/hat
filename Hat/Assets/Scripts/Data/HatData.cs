﻿using System;
using UnityEngine;

namespace Game {

    [Serializable]
    public sealed class HatData : AbstractData {

        public HatId id;

#if UNITY_EDITOR
        public string EditorName {
            get {
                return string.Format("Id = {0}: {1}", id.value, AbstractEnum.GetElementNameByValue<HatIdEnum>(id.value));
            }
        }
#endif
        public override int Id { get { return id.value; } }
    }
}

