﻿using System;
using UnityEngine;

namespace Game {

    [Serializable]
    public sealed class SkinData : AbstractData {

        public SkinId id;

#if UNITY_EDITOR
        public string EditorName {
            get {
                return string.Format("Id = {0}: {1}", id.value, AbstractEnum.GetElementNameByValue<SkinIdEnum>(id.value));
            }
        }
#endif
        public override int Id { get { return id.value; } }
    }
}

