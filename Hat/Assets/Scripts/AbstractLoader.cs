﻿using UnityEngine;

namespace Game {

    public abstract class AbstractLoader : MonoBehaviour {
        protected AbstractData[] _configData { get; set; }

        protected void SelectConfig(Stuff _stuff) {

            if (Stuff.Skin == _stuff) {
                _configData = SkinConfig.Instance.SkinsData;
            }

            if (Stuff.Hat == _stuff) {
                _configData = HatConfig.Instance.HatData;
            }

            if (Stuff.Robe == _stuff) {
                _configData = RobeConfig.Instance.RobeData;
            }

            if(Stuff.Money == _stuff) {
                _configData = MoneyConfig.Instance.MoneyData;
            }
        }


        protected Stuff DetectedStyffGroup(AbstractData data) {
            Stuff stuff = Stuff.none;
            if (data.GetType() == typeof(SkinData)) { stuff = Stuff.Skin; }
            if (data.GetType() == typeof(HatData)) { stuff = Stuff.Hat; }
            if (data.GetType() == typeof(RobeData)) { stuff = Stuff.Robe; }
            if (data.GetType() == typeof(MoneyData)) { stuff = Stuff.Money; }
            return stuff;
        }

        protected string DetectedKey(AbstractData data) {
            string stuffkey = "";
            if (data.GetType() == typeof(RobeData)) { stuffkey = AbstractEnum.GetElementNameByValue<RobeIdEnum>(data.Id); }
            if (data.GetType() == typeof(HatData)) { stuffkey = AbstractEnum.GetElementNameByValue<HatIdEnum>(data.Id); }
            if (data.GetType() == typeof(SkinData)) { stuffkey = AbstractEnum.GetElementNameByValue<SkinIdEnum>(data.Id); }
            if (data.GetType() == typeof(MoneyData)) { stuffkey = AbstractEnum.GetElementNameByValue<MoneyIdEnum>(data.Id); }
            return stuffkey;
        }
    }
}
