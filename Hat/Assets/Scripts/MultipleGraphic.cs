﻿using System.Linq;
using UnityEngine;
using UnityEngine.UI;
#pragma warning disable 649

namespace Game {
    public class MultipleGraphic : Graphic {
        [SerializeField] private Graphic[] _targets;

        public override Color color {
            set {
                foreach (var graphic in _targets)
                    graphic.color = value;
            }
            get {
                var target = _targets.FirstOrDefault();
                return target != null ? target.color : Color.white;
            }
        }

        public override void CrossFadeAlpha(float alpha, float duration, bool ignoreTimeScale) {
            foreach (var graphic in _targets)
                graphic.CrossFadeAlpha(alpha, duration, ignoreTimeScale);
        }

        public override void CrossFadeColor(Color targetColor, float duration, bool ignoreTimeScale, bool useAlpha) {
            foreach (var graphic in _targets)
                graphic.CrossFadeColor(targetColor, duration, ignoreTimeScale, useAlpha);
        }

        public override void CrossFadeColor(Color targetColor, float duration, bool ignoreTimeScale, bool useAlpha, bool useRGB) {
            foreach (var graphic in _targets)
                graphic.CrossFadeColor(targetColor, duration, ignoreTimeScale, useAlpha, useRGB);
        }
    }
}
