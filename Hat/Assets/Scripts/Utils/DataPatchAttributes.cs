using System;

namespace Utils {

    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
    public sealed class DataPatchClass : Attribute {

        public string OnPatchProcessStart { get; set; }
        public string OnPatchProcessed { get; set; }

        public DataPatchClass() {
        }
    }

    [AttributeUsage(AttributeTargets.Field)]
    public sealed class DataPatchExclude : Attribute {
    }

    [AttributeUsage(AttributeTargets.Field)]
    public sealed class DataPatchConditionField : Attribute {
    }

    [AttributeUsage(AttributeTargets.Field, AllowMultiple = true)]
    public sealed class DataPatchCondition : Attribute {
        public object Value { get; set; }

        public DataPatchCondition(object value) {
            Value = value;
        }
    }
}
