﻿using System;
using UnityEngine;

namespace Game {

    [Serializable]
    public sealed class MoneyData : AbstractData {

        public MoneyId id;

#if UNITY_EDITOR
        public string EditorName {
            get {
                return string.Format("Id = {0}: {1}", id.value, AbstractEnum.GetElementNameByValue<MoneyIdEnum>(id.value));
            }
        }
#endif
        [NonSerialized]
        public MoneyId _moneyId;
        public override int Id { get { return id.value; } }
    }
}

