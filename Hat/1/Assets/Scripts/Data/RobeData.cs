﻿using System;
using UnityEngine;

namespace Game {

    [Serializable]
    public sealed class RobeData : AbstractData {

        public RobeId id;

#if UNITY_EDITOR
        public string EditorName {
            get {
                return string.Format("Id = {0}: {1}", id.value, AbstractEnum.GetElementNameByValue<RobeIdEnum>(id.value));
            }
        }
#endif
        [NonSerialized]
        public RobeId _robeId;
        public override int Id { get { return id.value; }}
    }
}

