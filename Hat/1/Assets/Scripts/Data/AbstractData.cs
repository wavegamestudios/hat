﻿using System;
using UnityEngine;

public abstract class AbstractData {
    public Sprite sprite;
    public int price;
    public bool freeStuff;
    public virtual int Id { get; }
}
  
