﻿using Sirenix.OdinInspector;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = System.Object;

namespace Game {

    public abstract class AbstractEnum : ScriptableObject {

        public const int NONE_VALUE = 0;

        private static Dictionary<Type, AbstractEnum> _instances;
        public static T GetInstance<T>() where T : AbstractEnum 
        {
            if (_instances == null) 
                LoadInstances();

            if (!_instances.ContainsKey(typeof(T)))
                return null;

            return (T)_instances[typeof(T)];
        }

        private static void LoadInstances() {
            if (Application.isEditor) {
                _instances = new Dictionary<Type, AbstractEnum>();
                var enums = Resources.LoadAll<AbstractEnum>("");
                for (int i = 0; i < enums.Length; i++) {
                    var type = enums[i].GetType();
                    if (_instances.ContainsKey(type)) {
                        //Logger.LogErrorFormat("Duplicate enum instance: {0}!", type.ToString());
                    }
                    _instances[type] = enums[i];
                }
            } else {
                _instances = AbstractEnumStorage.Instance.GetEnums();
            }
        }

        public static bool IsNoneValue(int value) {
            return value == NONE_VALUE;
        }

        [ReadOnly]
        public int countValue;
        public bool reuseDeletedValues = true;

        [Serializable]
        public struct Element {

            [HideLabel, HorizontalGroup(MaxWidth = 0.15f), ReadOnly]
            public int value;
            [HideLabel, HorizontalGroup]
            public string name;
        }

        [ListDrawerSettings(Expanded = true, NumberOfItemsPerPage = 50, CustomAddFunction = "CreateNewElement")]
        public List<Element> elements = new List<Element>();


        public static string GetElementNameByValue<T>(int value, bool addId = false) where T : AbstractEnum {
            if (IsNoneValue(value)) {
                return "NONE";
            }
            foreach (var element in GetInstance<T>().elements) {
                if (element.value == value) {
                    return addId ? string.Format("{0} |> {1}", element.name, value) : element.name;
                }
            }
            return null;
        }
        
        public static int GetElementValueByName<T>(string elementName) where T : AbstractEnum {
            foreach (var element in GetInstance<T>().elements) {
                if (element.name == elementName)
                {
                    return element.value;
                }
            }
            return -1;
        }

        public static void SetOrAddValue<T>(int id, string value) where T: AbstractEnum
        {
            var elements = GetInstance<T>().elements;

            Element e = new Element();
            e.value = id;
            e.name = value;

            // Modify existing one
            for (int i=0; i< elements.Count; i++)
            {
                if(elements[i].value == id)
                {
                    if (elements[i].name != value)
                    {
                        GetInstance<T>().elements.RemoveAt(i);
                        GetInstance<T>().elements.Insert(i, e);
                    }
                    return;
                }
            }

            // Or add new one if none found
            GetInstance<T>().elements.Add(e);
            GetInstance<T>().countValue = Mathf.Max(GetInstance<T>().countValue, id);
        }

        public static Dictionary<string, int> GetValuesMap<T>() where T : AbstractEnum
        {
            Dictionary<string, int> Dict = new Dictionary<string, int>();

            foreach(var v in GetInstance<T>().elements)
            {
                Dict.Add(v.name, v.value);
            }

            return Dict;
        }

        public static Dictionary<string, int> GetValuesMapLowercase<T>() where T : AbstractEnum
        {
            Dictionary<string, int> Dict = new Dictionary<string, int>();

            foreach (var v in GetInstance<T>().elements)
            {
                Dict.Add(v.name.ToLower(), v.value);
            }

            return Dict;
        }

        public static IList GetDropdownValues<T>() where T : AbstractEnum {

            var none = new [] { new ValueDropdownItem("NONE", 0) };

            var element = GetInstance<T>().elements;

            var tes = none.Concat(GetInstance<T>().elements.Select(e => new ValueDropdownItem(string.Format("{0}   |>   id={1}", e.name, e.value), e.value))).ToList();

            return tes;
        }

        public Element CreateNewElement() {
            var resultValue = countValue;
            if (reuseDeletedValues) {
                var value = NONE_VALUE + 1;
                while (true) {
                    var free = true;
                    foreach (var e in elements) {
                        free &= e.value != value;
                    }
                    if (free) {
                        break;
                    }
                    value++;
                }
                resultValue = value;
                countValue = Mathf.Max(countValue, resultValue + 1);
            } else {
                countValue++;
            }
            return new Element() {
                value = resultValue,
                name = "New Element",
            };
        }
    }
}
