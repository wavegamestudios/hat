﻿using UnityEngine;

namespace Utils {

    public class SingletonCrossScene<T> : MonoBehaviour where T : MonoBehaviour {

        private static T _instance;
        public static T Instance {
            get {
                if (_instance == null) {
                    _instance = FindObjectOfType<T>();
                    if (_instance != null)
                    {
                        DontDestroyOnLoad(_instance);
                    }
                }
                return _instance;
            }
        }

        public static bool HasInstance()
        {
            return (_instance != null);
        }

        protected virtual void Awake() {
            _instance = GetComponent<T>();
            DontDestroyOnLoad(_instance);
        }
    }
}