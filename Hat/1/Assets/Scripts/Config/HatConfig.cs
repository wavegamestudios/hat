﻿using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace Game {

    [CreateAssetMenu(menuName = "Game/Configs/Hats Config")]
    public class HatConfig : ScriptableObjectSingleton<HatConfig> {
        [TabGroup("Hats")]
        [ListDrawerSettings(ListElementLabelName = "EditorName")]
        public HatData[] HatData;
    }
}
