﻿using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace Game {

    [CreateAssetMenu(menuName = "Game/Configs/Skins Config")]
    public class SkinConfig : ScriptableObjectSingleton<SkinConfig> {
        [TabGroup("Skins")]
        [ListDrawerSettings(ListElementLabelName = "EditorName")]
        public SkinData[] SkinsData;
    }
}
