﻿using Sirenix.OdinInspector;
using UnityEngine;
using Utils;

namespace Game {

    [CreateAssetMenu(menuName = "Game/Configs/Robes Config")]
    public class RobeConfig : ScriptableObjectSingleton<RobeConfig> {
        [TabGroup("Robes")]
        [ListDrawerSettings(ListElementLabelName = "EditorName")]
        public RobeData[] RobeData;
    }
}
