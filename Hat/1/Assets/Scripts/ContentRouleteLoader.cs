﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Game {

    public class ContentRouleteLoader : AbstractLoader {

        [SerializeField]
        private CellLogic _cellPrefab;

        [SerializeField]
        private GameObject _content;

        [SerializeField]
        private GameObject _contentPreview;

        private int _poolSize = 30;
        private readonly List<CellLogic> _poolList = new List<CellLogic>();
        private PoolContainer<CellLogic> _pool;

        private void Awake() {
            _pool = new PoolContainer<CellLogic>(_cellPrefab, _poolSize);
        }

        public void LoadCell(AbstractData data = null) {
            if (data != null) {
                LoadContent(data);
                LoadPreview(data);
            }
        }

        private void LoadPreview(AbstractData data = null) {

            var instance = _pool.GetItem(_contentPreview.transform);
            _poolList.Add(instance);

            instance.transform.localScale = Vector3.one;
            instance.LoadCellContent(data);
            StartCoroutine(ClearPreview(instance));
        }

        private void LoadContent(AbstractData data = null) {

            var instance = _pool.GetItem(_content.transform);
            _poolList.Add(instance);

            instance.transform.localScale = Vector3.one;
            instance.LoadCellContent(data);
        }

        private void ClearPool() {

            foreach (var cell in _poolList) {
                _pool.StoreItem(cell);
            }

            _poolList.Clear();
        }

        IEnumerator ClearPreview(CellLogic cell) {
            {
                yield return new WaitForSeconds(1.0f);
                _pool.StoreItem(cell);
            }
        }
    }
}
