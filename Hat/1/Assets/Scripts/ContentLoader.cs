﻿using System.Collections.Generic;
using UnityEngine;

namespace Game {

    public class ContentLoader : AbstractLoader {

        [SerializeField]
        private CellLogic _cellPrefab;

        [SerializeField]
        private GameObject _content;

        [SerializeField]
        private Stuff _stuff;

        private int _poolSize = 30;
        private readonly List<CellLogic> _poolList = new List<CellLogic>();
        private PoolContainer<CellLogic> _pool;

        private void Awake() {
            _pool = new PoolContainer<CellLogic>(_cellPrefab, _poolSize);
        }

        private void Start() {
            SelectConfig(_stuff);
            LoadCell(_configData);
        }

        private void LoadCell(AbstractData[] configData) {

            foreach (var config in configData) {
                LoadContent(config);
            }

            if (_poolSize - configData.Length > 0) {
                for (int i = 0; i < _poolSize - configData.Length; i++) {
                    LoadContent();
                }
            }
        }

        private void LoadContent(AbstractData data = null) {
            var instance = _pool.GetItem(_content.transform);
            _poolList.Add(instance);
            instance.transform.localScale = Vector3.one;
            instance.LoadCellContent(data);
        }

        private void ClearPool() {

            foreach (var cell in _poolList) {
                _pool.StoreItem(cell);
            }

            _poolList.Clear();
        }
    }
}
