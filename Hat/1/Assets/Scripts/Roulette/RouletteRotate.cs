﻿using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class RouletteRotate : MonoBehaviour {

    bool _run = false;
    float _degrees = 0;
    private int _spinValue = 99;

    public Action StopRouletteRotate;

    [SerializeField]
    private Button _runbutton;

    [SerializeField]
    private Text _spinCounterText;

    private void OnEnable() {
        _spinCounterText.text = _spinValue.ToString();
    }

    public void RunRoulete() {
        if (!_run) {
            _degrees = 3600 + UnityEngine.Random.Range(-100.0f, 100.0f);
            StartCoroutine(Rotate());
        }
    }

    private void Update() {
    
        if (_run) {

            _degrees -= 25 + UnityEngine.Random.Range(-10.0f, 10.0f);
            
            if (_degrees > 0) {
                Vector3 to = new Vector3(0, 0, _degrees);
                transform.eulerAngles = Vector3.Lerp(transform.rotation.eulerAngles, to, Time.deltaTime);               
            } else {
                _run = false;            
            }
        }      

        if(_spinValue <= 0) {
            _runbutton.interactable = false;
        }
    }

    IEnumerator Rotate() {
        _run = true;  
        _runbutton.interactable = false;
        _spinValue -= 1;
        _spinCounterText.text = _spinValue.ToString();
        yield return new WaitForSeconds(3.0f);
        _run = false;
        _runbutton.interactable = true;
        StopRouletteRotate.Invoke();
    }
}
