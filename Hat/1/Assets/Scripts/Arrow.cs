﻿using UnityEngine;

public class Arrow : MonoBehaviour {
    private Collider2D _lastSector { get; set; }
    public Collider2D LastSector { get { return _lastSector; } }

    void OnTriggerEnter2D(Collider2D other) {
        _lastSector = other;
    }
}
