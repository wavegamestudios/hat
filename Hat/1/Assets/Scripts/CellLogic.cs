﻿using UnityEngine;
using UnityEngine.UI;

namespace Game {

    public class CellLogic : AbstractLoader {

        [SerializeField]
        private RawImage _stuffImage;

        [SerializeField]
        private GameObject _priceOb;

        [SerializeField]
        private Text _priceText;

        [SerializeField]
        private GameObject _noActiveCell;

        [SerializeField]
        private GameObject _lockObject;

        [SerializeField]
        private Button _clickCell;

        private AbstractData _data { get; set; }

        private Wardrobe _wardrobe { get; set; }

        private Player _player { get; set; }

        private void Awake() {
            _wardrobe = (Wardrobe)FindObjectOfType(typeof(Wardrobe));
            _player = (Player)FindObjectOfType(typeof(Player));
            _clickCell.onClick.AddListener(TaskOnClick);
        }

        private void OnEnable() {
            UnlockCell(_data);
        }

        public void LoadCellContent(AbstractData data = null) {

            if (data == null) {
                _priceOb.SetActive(false);
                _lockObject.SetActive(true);
                return;
            };

            _stuffImage.texture = data.sprite.texture;

            if (DetectedStyffGroup(data) == Stuff.Money) {
                _priceOb.SetActive(true);
                _priceText.text = "+" + data.price.ToString();
            } else {
                _priceText.text = data.price.ToString();
            }
        
            _data = data;

            UnlockCell(data);
        }

        private void UnlockCell(AbstractData data) {
            if (data != null && FindStuffInDrop(data)) {

                if (DetectedStyffGroup(data) != Stuff.Money)
                    _priceOb.SetActive(false);
                
                _lockObject.SetActive(false);
                _noActiveCell.SetActive(false);
            }
        }

        public bool FindStuffInDrop(AbstractData data) {
            if (_wardrobe == null) return false;
            return _wardrobe.WardrodeList.ContainsKey(DetectedKey(data));
        }

        void TaskOnClick() {
            if (_data != null && _player != null) {
                _player.actionData(_data);
                UnlockCell(_data);
            }
        }
    }
}
