﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Game {

    public class Shop : AbstractLoader {

        [SerializeField]
        private Bank _bank;


        public Dictionary<string, Stuff> ShopList = new Dictionary<string, Stuff>();
        public Dictionary<string, Stuff> SoldList = new Dictionary<string, Stuff>();

        // Start is called before the first frame update
        private void Awake() {

            foreach (Stuff stuff in Enum.GetValues(typeof(Stuff))) {
                {
                    if (Stuff.none != stuff) {
                        SelectConfig(stuff);
                        LoadStuff(_configData);
                    }
                }
            }
        }

        private void LoadStuff(AbstractData[] configData) {

            foreach (var config in configData) {

                var key = DetectedKey(config);

                if (config.freeStuff) {
                    if (!SoldList.ContainsKey(key)) SoldList.Add(key, DetectedStyffGroup(config));
                } else {
                    if (!ShopList.ContainsKey(key)) ShopList.Add(DetectedKey(config), DetectedStyffGroup(config));
                }
            }
        }

        public void BuyStuff(AbstractData data, Action update) {

            if (_bank.GetMoney() - data.price >= 0) {
                _bank.SetMoney(_bank.GetMoney() - data.price);
                MakePurchase(data);
                update.Invoke();
            }
        }

        public void MakePurchase(AbstractData data) {
            var key = DetectedKey(data);
            var stuffGroup = DetectedStyffGroup(data);

            PassStuff(key, stuffGroup);
        }

        public void PassStuff(string key, Stuff stuffGroup) {
            if (!SoldList.ContainsKey(key)) SoldList.Add(key, stuffGroup);
            if (ShopList.ContainsKey(key)) ShopList.Remove(key);
        }
    }
}
