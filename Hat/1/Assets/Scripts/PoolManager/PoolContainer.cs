﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Object = UnityEngine.Object;
// ReSharper disable PossibleNullReferenceException

namespace Game {
    public class PoolContainer<T> where T : MonoBehaviour {
        private readonly Queue<MonoBehaviour> _pool;
        private readonly MonoBehaviour _prefab;

        public PoolContainer(MonoBehaviour prefab, int preInit = 0) {
            if (prefab == null)
                throw new ArgumentException("[PoolManager] Prefab is null.");
            _pool = new Queue<MonoBehaviour>();
            _prefab = prefab;
            for (var i = 0; i < preInit; i++)
                StoreItem(GetItem());
        }


        public T GetItem(Transform transform = null, bool worldPositionStays = false) {
            MonoBehaviour item;
            if (_pool.Any() && (item = _pool.Dequeue()) != null)
                item.gameObject.transform.SetParent(transform, worldPositionStays);
            else
                item = Object.Instantiate(_prefab, transform, worldPositionStays);
            item.gameObject.SetActive(true);
            return (T)item;
        }

        public void StoreItem(Transform itemToStore, bool worldPositionStays = false) {
            T item;
            if ((item = itemToStore.GetComponent<T>()) == null)
                throw new ArgumentException("[PoolManager] Invalid type.");
            StoreItem(item, worldPositionStays);
        }

        public void StoreItem(T item, bool worldPositionStays = false) {
            item.gameObject.SetActive(false);
            item.gameObject.transform.SetParent(null, worldPositionStays);
            _pool.Enqueue(item);
        }
    }
}
