﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Game {
    public class Player : AbstractLoader {

        [SerializeField]
        private Image _skinImage;

        [SerializeField]
        private Image _hatImage;

        [SerializeField]
        private Image _robeImage;

        [SerializeField]
        private Wardrobe _wardrobe;

        [SerializeField]
        private Shop _shop;

        public Action<AbstractData> actionData;

        private void Awake() {
            actionData += UpdateStuff;
            LoadStuffInWardrobe();
            LoadFirstSoldStuff();
        }

        private void LoadStuffInWardrobe() {
            if (_wardrobe != null && _shop != null) {
                foreach (var key in _shop.SoldList.Keys) {
                    if (!_wardrobe.WardrodeList.ContainsKey(key)) {
                        _wardrobe.WardrodeList.Add(key, _shop.SoldList[key]);
                    }
                }
            }
        }

        private void LoadFirstSoldStuff() {
            foreach (var key in _wardrobe.WardrodeList.Keys) {
                var data = FindData(key, _wardrobe.WardrodeList[key]);
                if (data != null) {
                    UpdateStuff(data);
                }
            }
        }

        private void UpdateStuff(AbstractData data) {

            var stuffkey = DetectedKey(data);
            var stuffGroup = DetectedStyffGroup(data);

            if (_wardrobe.WardrodeList.ContainsKey(stuffkey) && _wardrobe.WardrodeList.ContainsValue(stuffGroup)) {
                if (stuffGroup == Stuff.Skin) { _skinImage.sprite = data.sprite; }
                if (stuffGroup == Stuff.Hat) { _hatImage.sprite = data.sprite; }
                if (stuffGroup == Stuff.Robe) { _robeImage.sprite = data.sprite; }
            } else {
                _shop.BuyStuff(data, LoadStuffInWardrobe);
            }
        }

        private AbstractData FindData(string key, Stuff stuffGroup) {

            foreach (Stuff stuff in Enum.GetValues(typeof(Stuff))) {
                {
                    if (Stuff.none != stuff) {
                        SelectConfig(stuff);
                        foreach (var data in _configData) {

                            if (DetectedKey(data) == key && DetectedStyffGroup(data) == stuffGroup) {
                                return data;
                            }
                        }
                    }
                }
            }

            return null;
        }
    }
}
