﻿using UnityEngine;
using UnityEngine.UI;

public class Money : MonoBehaviour {

    [SerializeField]
    private Text _moneyText;

    [SerializeField]
    private Bank _bank;
   
    // Update is called once per frame
    void Update() {
        if (_moneyText != null && _bank != null) {
            _moneyText.text = _bank.GetMoney().ToString();
        }
    }
}
