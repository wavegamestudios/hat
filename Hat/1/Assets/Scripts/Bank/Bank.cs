﻿using UnityEngine;

public class Bank : MonoBehaviour {

    private int _money = 200;
    public void SetMoney(int money) {

        if (money >= 0) {
            _money = money;
        } else {
            _money = 0;
        }
    }
    public int GetMoney() {
        return _money;
    }
}
